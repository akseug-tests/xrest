package com.akseug.test.xrest.controller;

import com.akseug.test.xrest.XrestRuntimeException;

/**
 * Exception for handle error in controller.
 * <p><b>WARN: </b>This is RuntimeException!
 * <p>Created by axenty on 4/14/17.
 */
public class RestRuntimeException extends XrestRuntimeException {

    public RestRuntimeException() {
    }

    public RestRuntimeException(String message) {
        super(message);
    }

    public RestRuntimeException(String message, Throwable cause) {
        super(message, cause);
    }

    public RestRuntimeException(Throwable cause) {
        super(cause);
    }

    public RestRuntimeException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
