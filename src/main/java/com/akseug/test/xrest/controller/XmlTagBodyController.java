package com.akseug.test.xrest.controller;

import com.akseug.test.xrest.XrestRuntimeException;
import com.akseug.test.xrest.domain.ErrorResponse;
import com.akseug.test.xrest.domain.XmlTagBodyResponse;
import com.akseug.test.xrest.service.ServiceRuntimeException;
import com.akseug.test.xrest.service.XmlTagBodyService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

/**
 * XmlTagBodyController is rest controller.
 *
 * Created by axenty on 4/14/17.
 */
@RestController
public class XmlTagBodyController {

    private static final Logger LOGGER = LoggerFactory.getLogger(XmlTagBodyController.class);

    @Autowired
    XmlTagBodyService xmlTagBodyService;

    @RequestMapping(value = "/tagbody", produces = {"application/xml", "application/json"}, method = RequestMethod.GET)
    public XmlTagBodyResponse getXmlTagBodyResponse(@RequestParam(value = "name", required = false) String name) {
        XmlTagBodyResponse response = new XmlTagBodyResponse();
        response.setTimestamp(System.currentTimeMillis());
        response.setName(name);
        response.setBody(xmlTagBodyService.findBodyByTag(name));
        return response;
    }

    @RequestMapping(value = "/tagbody", method = RequestMethod.GET)
    public ResponseEntity<String> getTagBody(@RequestParam(value = "name", required = false) String name) {
        return new ResponseEntity<>(xmlTagBodyService.findBodyByTag(name), HttpStatus.OK);
    }

    @RequestMapping(value = {"/tagbody"}, produces = {"application/xml", "application/json"})
    public void stubRestMethods() { //stub on other methods (PUT, POST, DELETE, etc)
        throw new RestRuntimeException("Not implemented!");
    }


    @ExceptionHandler(XrestRuntimeException.class)
    public ResponseEntity<ErrorResponse> handleException(HttpServletRequest request, XrestRuntimeException ex) {
        printException(ex);
        ErrorResponse errorResponse = new ErrorResponse();
        errorResponse.setTimestamp(System.currentTimeMillis());
        errorResponse.setMessage(ex.getMessage());
        errorResponse.setPath(request.getServletPath());
        errorResponse.setCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
        return new ResponseEntity<>(errorResponse, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    private void printException(XrestRuntimeException ex) {
        if (ex instanceof RestRuntimeException) {
            LOGGER.debug(ex.getMessage(), ex);
        } else if (ex instanceof ServiceRuntimeException) {
            LOGGER.error(ex.getMessage(), ex);
        } else {
            LOGGER.warn(ex.getMessage(), ex);
        }
    }

}
