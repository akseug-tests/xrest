package com.akseug.test.xrest.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.IOException;
import java.io.StringWriter;

/**
 * The service read xml file, parse to Document, find need tag by name and return body for this tag.
 * Use argument <b><i>--xrest.xmlfile</i></b> that set path to file.
 * <p>Created by axenty on 4/14/17.
 */
@Service
public class FileXmlTagBodyServiceImpl implements XmlTagBodyService {

    private final String filePath;

    @Autowired
    public FileXmlTagBodyServiceImpl(@Value("${xrest.xmlfile}") String filePath) {
        this.filePath = filePath;
    }

    @Override
    public String findBodyByTag(String tagName) {
        Document document = parseXml();
        if (StringUtils.isEmpty(tagName)) {
            return serializeXml(document.getChildNodes());
        } else {
            NodeList nList = document.getElementsByTagName(tagName);
            return serializeXml(nList.item(0).getChildNodes());
        }
    }

    private Document parseXml() {
        try {
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            Document doc = db.parse(filePath);
            doc.getDocumentElement().normalize();
            return doc;
        } catch (ParserConfigurationException | SAXException | IOException e) {
            throw new ParseXmlRuntimeException(e.getMessage(), e);
        }

    }

    private String serializeXml(NodeList nodeList) {
        try {
            TransformerFactory tf = TransformerFactory.newInstance();
            Transformer transformer = tf.newTransformer();
            transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
            StringWriter writer = new StringWriter();
            StreamResult streamResult = new StreamResult(writer);
            for (int i = 0; i < nodeList.getLength(); i++) {
                transformer.transform(new DOMSource(nodeList.item(i)), streamResult);
            }
            return writer.getBuffer().toString();
        } catch (TransformerException e) {
            throw new ServiceRuntimeException(e.getMessage(), e);
        }
    }
}
