package com.akseug.test.xrest.service;

import com.akseug.test.xrest.XrestRuntimeException;

/**
 * Common service exception.
 *
 * <p><b>WARN: </b>This is RuntimeException!
 * <p>Created by axenty on 4/14/17.
 */
public class ServiceRuntimeException extends XrestRuntimeException {
    public ServiceRuntimeException() {
    }

    public ServiceRuntimeException(String message) {
        super(message);
    }

    public ServiceRuntimeException(String message, Throwable cause) {
        super(message, cause);
    }

    public ServiceRuntimeException(Throwable cause) {
        super(cause);
    }

    public ServiceRuntimeException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
