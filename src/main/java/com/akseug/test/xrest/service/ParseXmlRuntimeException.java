package com.akseug.test.xrest.service;

/**
 * <p><b>WARN: </b>This is RuntimeException!
 * <p>Created by axenty on 4/14/17.
 */
public class ParseXmlRuntimeException extends ServiceRuntimeException {

    public ParseXmlRuntimeException() {
    }

    public ParseXmlRuntimeException(String message) {
        super(message);
    }

    public ParseXmlRuntimeException(String message, Throwable cause) {
        super(message, cause);
    }

    public ParseXmlRuntimeException(Throwable cause) {
        super(cause);
    }

    public ParseXmlRuntimeException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
