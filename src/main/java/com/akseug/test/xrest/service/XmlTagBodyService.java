package com.akseug.test.xrest.service;

/**
 * Inteface of service for finding tags in xml.
 *
 * <p>Created by axenty on 4/14/17.
 */
public interface XmlTagBodyService {

    String findBodyByTag(String tagName);

}
