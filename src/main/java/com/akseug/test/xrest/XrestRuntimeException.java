package com.akseug.test.xrest;

/**
 * Common runtime exception inside Xrest app.
 *
 * <p><b>WARN: </b>This is RuntimeException!
 * <p>Created by axenty on 4/14/17.
 */
public class XrestRuntimeException extends RuntimeException {

    public XrestRuntimeException() {
    }

    public XrestRuntimeException(String message) {
        super(message);
    }

    public XrestRuntimeException(String message, Throwable cause) {
        super(message, cause);
    }

    public XrestRuntimeException(Throwable cause) {
        super(cause);
    }

    public XrestRuntimeException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
