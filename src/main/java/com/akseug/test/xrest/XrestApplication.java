package com.akseug.test.xrest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class XrestApplication {

	public static void main(String[] args) {
		SpringApplication.run(XrestApplication.class, args);
	}
}
