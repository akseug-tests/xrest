package com.akseug.test.xrest.domain;

import javax.xml.bind.annotation.*;

/**
 * Created by axenty on 4/14/17.
 */
@XmlRootElement(name = "tagBodyResponse")
@XmlAccessorType(XmlAccessType.NONE)
public class XmlTagBodyResponse {

    @XmlElement(name = "name")
    private String name;

    @XmlElement(name = "body")
    private String body;

    @XmlAttribute(name = "timestamp")
    private long timestamp;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("XmlTagBodyResponse{");
        sb.append("name='").append(name).append('\'');
        sb.append(", body='").append(body).append('\'');
        sb.append(", timestamp=").append(timestamp);
        sb.append('}');
        return sb.toString();
    }
}
