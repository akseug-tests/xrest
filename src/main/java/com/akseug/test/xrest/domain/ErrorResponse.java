package com.akseug.test.xrest.domain;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by axenty on 4/14/17.
 */
@XmlRootElement(name = "errorResponse")
@XmlAccessorType(XmlAccessType.NONE)
public class ErrorResponse {
    @XmlElement(name = "timestamp")
    private long timestamp;

    @XmlElement(name = "path")
    private String path;

    @XmlElement(name = "message")
    private String message;

    @XmlElement(name = "code")
    private int code;

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("ErrorResponse{");
        sb.append("timestamp=").append(timestamp);
        sb.append(", path='").append(path).append('\'');
        sb.append(", message='").append(message).append('\'');
        sb.append(", code=").append(code);
        sb.append('}');
        return sb.toString();
    }
}
